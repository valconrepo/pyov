## Installation

Copy OvationDataAPIWrapper.dll  to the Ovation/OvationBase. DLL file can be found here:
https://valconinternational-my.sharepoint.com/:u:/g/personal/naujokas_valcon-int_com/Ee4SrCv3lttCg5pipPS95r4BrdY0jKs24AQMptssAjoIeA?e=qusWeq 

Copy PyOv.py to working directory

## Dependencies

Use pip to install win32 libraries. Libraries are use to correctly load .dll files. Otherwise .dll hijacking is expected.

```bash
pip install pypiwin32
```

You can also copy files win32api.pyd and wind32con.py to working directory


## Usage

Make scripts in the same folder

```python
from PyOv import *

OvationGateway = PyOv()
AnalogPoint = "TYPICAL_HIC2-DMD.UNIT0@NET0"
sidAnalog = OvationGateway.GetPointSID(AnalogPoint)
print(f"SID of point {AnalogPoint} is {sidAnalog}")
OvationDataAnalog = OvationGateway.GetAnalogValue(sidAnalog)
print (f"Value of point {AnalogPoint} is {OvationDataAnalog.value}")
print (f"Status of point {AnalogPoint} is {OvationDataAnalog.status}")
print (f"Timestamp of point {AnalogPoint} is {OvationDataAnalog.time}")
DigitalPoint = "TYPICAL_HIC2XB06.UNIT0@NET0"
sidDigital = OvationGateway.GetPointSID(DigitalPoint)
print(f"SID of point {DigitalPoint} is {sidDigital}")
OvationDataDigital = OvationGateway.GetDigitalValue(sidDigital)
print (f"Value of point {DigitalPoint} is {OvationDataDigital.value}")
print (f"Status of point {DigitalPoint} is {OvationDataDigital.status}")
print (f"Timestamp of point {DigitalPoint} is {OvationDataDigital.time}")
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

